__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path
from typing import List
from unittest.mock import patch

from mafe.scan import Scanner, TrackData
from .data_dirs import ogg_dir, silence_dir
from .with_storage import WithStorage


class ScanTestCase(WithStorage):

    def test_audio_files_recognized(self):
        scanner = Scanner(self.out_path, [ogg_dir], quiet=True)
        for file in (
                'default/private.ogg', 'default/room_nick.ogg',
                'cough/private.ogg', 'cough/room_nick.ogg'
        ):
            self.assertTrue(self.is_file_in_list(scanner.files, file))
        for file in ('default/license.txt', 'cough/license.txt'):
            self.assertFalse(self.is_file_in_list(scanner.files, file))

    def test_run_succeeds(self):
        scanner = Scanner(self.out_path, [ogg_dir], quiet=True)
        result = scanner.run()
        self.assertFalse(result.empty)
        self.assertTrue(self.out_path.exists())

    def test_track_over_length_limit_is_ignored(self):
        TrackData.set_max_track_duration(240)
        scanner = Scanner(self.out_path, [silence_dir], quiet=True, )
        result = scanner.run()
        self.assertTrue(result.empty, result.to_string())

    @patch('librosa.beat.beat_track', side_effect=ValueError())
    def test_librosa_value_error_is_caught(self, _):
        scanner = Scanner(self.out_path, [ogg_dir], quiet=True)
        result = scanner.run()
        self.assertTrue(result.empty)

    @patch('librosa.load', side_effect=ZeroDivisionError())
    def test_librosa_zero_division_error_is_caught(self, _):
        scanner = Scanner(self.out_path, [ogg_dir], quiet=True)
        result = scanner.run()
        self.assertTrue(result.empty)

    @staticmethod
    def is_file_in_list(path_list: List[Path], path_part: str) -> bool:
        return bool([path for path in path_list if path_part in str(path)])
