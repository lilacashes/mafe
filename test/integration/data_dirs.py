__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import os

data_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')
sound_dir = os.path.join(data_dir, 'sound')
ogg_dir = os.path.join(sound_dir, 'ogg')
mp3_dir = os.path.join(sound_dir, 'mp3')
silence_dir = os.path.join(data_dir, 'silence')
