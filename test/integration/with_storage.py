__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import unittest
from pathlib import Path
from tempfile import TemporaryDirectory

from test.integration.data_dirs import ogg_dir, sound_dir

import pandas as pd

from mafe.process import normalize
from mafe.scan import Scanner


class WithStorage(unittest.TestCase):

    def setUp(self) -> None:
        module_name = Path(__file__).stem
        self.temp_dir = TemporaryDirectory()
        self.out_path = Path(self.temp_dir.name, f'test_{module_name}.csv.bz2')

    def tearDown(self) -> None:
        self.temp_dir.cleanup()

    def create_normalized_csv(self):
        tracks = self.read_tracks(sound_dir)
        self.assertEqual(8, tracks.shape[0], tracks[['hash', 'file_name']].to_string())
        normalized = normalize(tracks)
        self.assertEqual(8, normalized.shape[0], normalized.to_string())
        normalized.to_csv(self.out_path)

    def read_tracks(self, dir_to_read: str = ogg_dir) -> pd.DataFrame:
        scanner = Scanner(str(self.out_path.absolute()), [dir_to_read], quiet=True)
        return scanner.run()
