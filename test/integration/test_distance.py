__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from pathlib import Path

import pandas as pd

from mafe.process import DistanceCalculator, distances, normalize
from mafe.scan import Scanner
from .data_dirs import ogg_dir, sound_dir
from .with_storage import WithStorage


class DistanceTestCase(WithStorage):

    def test_calculate_distances(self):
        tracks = self.read_tracks()
        self.assertEqual(4, tracks.shape[0], tracks[['hash', 'file_name']].to_string())
        self.check_distances_are_consistent(distances(tracks), 4)

    def test_distance_with_normalization(self):
        tracks = self.read_tracks()
        self.assertEqual(4, tracks.shape[0], tracks[['hash', 'file_name']].to_string())
        normalized = normalize(tracks)
        self.assertEqual(4, normalized.shape[0], normalized.to_string())
        self.check_distances_are_consistent(distances(normalized), 4)

    def test_with_different_track_versions(self):
        tracks = self.read_tracks(sound_dir)
        self.assertEqual(8, tracks.shape[0], tracks[['hash', 'file_name']].to_string())
        normalized = normalize(tracks)
        self.assertEqual(8, normalized.shape[0], normalized.to_string())
        self.check_distances_are_consistent(distances(normalized), 8)

    def test_distance_calculator(self):
        self.create_normalized_csv()
        distances_path = Path(self.temp_dir.name, 'test_reduced.csv.bz2')
        calculator = DistanceCalculator(self.out_path, distances_path)
        calculator.run(verbose=True)
        distance = pd.read_csv(distances_path)
        self.check_distances_are_consistent(distance, 8)

    def check_distances_are_consistent(self, distance: pd.DataFrame, data_length: int):
        self.assertEqual(
            (data_length - 1) * data_length // 2, distance.shape[0], distance.to_string()
        )
        self.assertTrue(that_no_two_hashes_are_equal(distance))
        self.assertTrue(that_distances_are_greater_zero(distance))
        self.assertTrue(that_every_pair_of_hashes_appears_only_once(distance))

    def read_tracks(self, dir_to_read: str = ogg_dir) -> pd.DataFrame:
        scanner = Scanner(str(self.out_path.absolute()), [dir_to_read], quiet=True)
        return scanner.run()


def that_no_two_hashes_are_equal(distance: pd.DataFrame) -> bool:
    equal_hashes = distance[distance['hash_1'] == distance['hash_2']]
    return equal_hashes.empty


def that_distances_are_greater_zero(distance: pd.DataFrame) -> bool:
    bad_distances = distance[distance['process'] <= 0]
    return bad_distances.empty


def that_every_pair_of_hashes_appears_only_once(distance: pd.DataFrame) -> bool:
    hashes = [(pair[0], pair[1]) for pair in distance[['hash_1', 'hash_2']].values.tolist()]
    reverse_hashes = [(pair[1], pair[0]) for pair in hashes]
    has_dups = len(hashes) != len(set(hashes))
    has_reverse_dups = bool(set(hashes).intersection(set(reverse_hashes)))
    return not has_dups and not has_reverse_dups
