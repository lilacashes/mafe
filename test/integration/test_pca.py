__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from test.integration.with_storage import WithStorage
from mafe.process.pca import Reducer

MIN_NUM_FEATURES = 500


class PCATestCase(WithStorage):

    def test_with_2_different_encodings_of_4_tracks(self):
        self.create_normalized_csv()
        reducer = Reducer(self.out_path, total_variance_ratio=0.99)
        self.assertEqual(8, reducer.tracks.shape[0])
        self.assertGreaterEqual(reducer.tracks.shape[1], MIN_NUM_FEATURES)

        reduced = reducer.run()
        self.assertEqual(8, reduced.shape[0])
        self.assertLess(reduced.shape[1], MIN_NUM_FEATURES)
        self.assertLess(len(reducer.explained_variance_ratio), MIN_NUM_FEATURES)
        self.assertLess(sum(reducer.explained_variance_ratio), 1)
        self.assertGreaterEqual(sum(reducer.explained_variance_ratio), 0.99)

    def test_small_variance(self):
        self.create_normalized_csv()
        reducer = Reducer(self.out_path, total_variance_ratio=0.5)
        reduced = reducer.run()
        self.assertEqual(8, reduced.shape[0])
        self.assertLess(reduced.shape[1], 10)
        self.assertLess(sum(reducer.explained_variance_ratio), 1)
        self.assertGreaterEqual(sum(reducer.explained_variance_ratio), 0.5)
