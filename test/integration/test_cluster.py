__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from contextlib import contextmanager
from os import environ
from pathlib import Path

import pandas as pd

from mafe.process.cluster import Cluster
from mafe.process.pca import Reducer
from .with_storage import WithStorage


class ClusterTestCase(WithStorage):

    def setUp(self) -> None:
        super().setUp()
        self.reduced_path = Path(self.temp_dir.name, 'test_reduced.csv.bz2')
        self.visualization_path = Path(self.temp_dir.name, 'test_visualize.svg')
        self.create_normalized_csv()

    def test_with_2_different_encodings_of_4_tracks(self):
        clustered = Cluster(self.out_path, 4).run()

        self.assert_num_clusters_is(clustered, 4)
        for cluster in range(4):
            self.assert_n_files_same_stem_different_extension(2, cluster, clustered)

    def test_visualization(self):
        Reducer(self.out_path, total_variance_ratio=0.99).run().to_csv(self.reduced_path)
        cluster = Cluster(self.reduced_path, 4)
        cluster.run()
        cluster.visualize(image_file=self.visualization_path)
        self.assertTrue(self.visualization_path.exists())

    def test_visualization_misused(self):
        cluster = Cluster(self.out_path, 4)
        cluster.run()
        with self.assertRaises(ValueError, msg="Visualizing without dimensionality reduction"):
            cluster.visualize(image_file=self.visualization_path)

        Reducer(self.out_path).run().to_csv(self.reduced_path)
        cluster = Cluster(self.reduced_path, 4)
        with self.assertRaises(ValueError, msg="Visualizing without clustering"):
            cluster.visualize(image_file=self.visualization_path)

        Reducer(self.out_path).run().to_csv(self.reduced_path)
        cluster = Cluster(self.reduced_path, 4)
        cluster.run()
        with unset_envvar('DISPLAY'):
            with self.assertRaises(ValueError, msg="Visualizing on headless without image file"):
                cluster.visualize(image_file=None)

    def assert_n_files_same_stem_different_extension(
            self, num_equal_files: int, cluster_id: int, clustered: pd.DataFrame
    ):
        files = clustered[clustered['cluster'] == cluster_id]
        self.assertEqual(num_equal_files, files.shape[0])
        stems = [Path(path).stem for path in files['file_name']]
        exts = [Path(path).suffix for path in files['file_name']]
        self.assertEqual(1, len(set(stems)))
        self.assertEqual(num_equal_files, len(set(exts)))

    def assert_num_clusters_is(self, clustered: pd.DataFrame, number: int):
        self.assertEqual(0, clustered['cluster'].min())
        self.assertEqual(number - 1, clustered['cluster'].max())


@contextmanager
def unset_envvar(envvar: str):
    saved = environ.get(envvar)
    if saved is not None:
        del environ[envvar]
    try:
        yield
    finally:
        if saved is not None:
            environ[envvar] = saved
