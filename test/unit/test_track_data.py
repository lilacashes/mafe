__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import unittest
from unittest.mock import Mock, patch

from mafe.scan.track_data import TrackDataDuck, TrackData


class TrackDataTestCase(unittest.TestCase):

    def test_track_data_duck_methods_raise_not_implemented_error(self):
        track_data = TrackDataDuck()
        with self.assertRaises(NotImplementedError):
            self.assertIsNone(track_data.data_frame)
        with self.assertRaises(NotImplementedError):
            self.assertIsNone(track_data.hash)

    @patch('mafe.scan.track_data.TrackData.load_track')
    def test_hash_before_metadata_is_read(self, load_track):
        track = TrackData(Mock(), Mock())
        load_track.assert_called()
        self.assertIsNone(track.hash)
