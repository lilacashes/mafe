__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import unittest
from unittest.mock import Mock, PropertyMock, patch
from hashlib import sha3_224
from pathlib import Path
from tempfile import NamedTemporaryFile
from typing import Tuple

import pandas as pd

from mafe.scan.track_data_storage import TrackDataStorage


class TrackDataStorageTestCase(unittest.TestCase):

    def test_open_empty_csv(self):
        storage = _empty_storage()
        self.assertTrue(storage.tracks.empty, storage.tracks)

    def test_load_tracks(self):
        csv_file = _create_csv()
        storage = TrackDataStorage(Path(csv_file.name))
        self._contains_values_in_range(storage.tracks, 2)

    def test_add_tracks_as_dataframe(self):
        csv_file = _create_csv()
        storage = TrackDataStorage(Path(csv_file.name))

        additional_tracks = _create_dataframe((2, 3))
        storage.add_dataframe(additional_tracks)

        self._contains_values_in_range(storage.tracks, 4)

    def test_add_tracks_with_duplicates(self):
        csv_file = _create_csv()
        storage = TrackDataStorage(Path(csv_file.name))

        additional_tracks = _create_dataframe((1, 2))
        storage.add_dataframe(additional_tracks)

        self._contains_values_in_range(storage.tracks, 3)

    def test_add_track(self):
        storage = _empty_storage()
        self.assertEqual(0, storage.num_buffered)
        storage.add_track(Mock())
        self.assertEqual(1, storage.num_buffered)

    def test_from_hash(self):
        csv_file = _create_csv()
        storage = TrackDataStorage(Path(csv_file.name))
        row = storage.from_hash(_hash(0))
        self.assertEqual(row.shape[0], 1)
        self.assertEqual(list(row['value'])[0], 0)
        row = storage.from_hash(_hash(1))
        self.assertEqual(row.shape[0], 1)
        self.assertEqual(list(row['value'])[0], 1)
        row = storage.from_hash(_hash(2))
        self.assertIsNone(row)

    def test_from_hash_empty_csv(self):
        with NamedTemporaryFile(mode='r') as empty_file:
            storage = TrackDataStorage(Path(empty_file.name))
            row = storage.from_hash('nonexistent')
        self.assertIsNone(row)

    @patch('mafe.scan.track_data_storage.TrackDataStorage.store_set')
    def test_tracks_are_saved_when_used_as_context_manager(self, store_set):
        with _empty_storage() as storage:
            storage.add_track(Mock())
            self.assertEqual(1, storage.num_buffered)
        store_set.assert_called()
        self.assertEqual(0, storage.num_buffered)

    @patch('mafe.scan.track_data_storage.TrackDataStorage.store_set')
    def test_tracks_are_saved_when_buffer_is_full(self, store_set):
        storage = _empty_storage(store_every=2)
        storage.add_track(Mock())
        store_set.assert_not_called()
        self.assertEqual(1, storage.num_buffered)
        storage.add_track(Mock())
        store_set.assert_called()
        self.assertEqual(0, storage.num_buffered)

    def test_buffer_is_merged_into_all_tracks_after_saving(self):
        new_track_dataframe = _create_dataframe((2,))
        new_track = PropertyMock(data_frame=new_track_dataframe)
        csv_file = _create_csv()
        with TrackDataStorage(Path(csv_file.name)) as storage:
            self._contains_values_in_range(storage.tracks, 2)
            storage.add_track(new_track)
        self._contains_values_in_range(storage.tracks, 3)

    def test_from_hash_reads_buffer_and_all_tracks(self):
        csv_file = _create_csv()
        storage = TrackDataStorage(Path(csv_file.name))
        self._contains_values_in_range(storage.tracks, 2)
        new_track = _mock_track(3)
        storage.add_track(new_track)
        from_hash = storage.from_hash(_hash(3))
        self.assertIsNotNone(from_hash)
        self.assertTrue(_create_dataframe((3,)).equals(from_hash))

    def test_store_set(self):
        tracks = {_mock_track(0), _mock_track(1)}
        storage = _empty_storage()
        self.assertTrue(storage.tracks.empty)
        storage.store_set(tracks)
        self._assert_has_stored(storage)

    def test_store_dataframe(self):
        data = _create_dataframe((1, 2))
        storage = _empty_storage()
        self.assertTrue(storage.tracks.empty)
        storage.store_dataframe(data)
        self._assert_has_stored(storage)

    def _contains_values_in_range(self, frame: pd.DataFrame, range_max: int) -> None:
        self.assertEqual(frame.shape[0], range_max)
        for i in range(range_max):
            self.assertEqual(self._get_value(frame, i), i)
        self.assertTrue(_get_hash(frame, range_max).empty)

    def _assert_has_stored(self, storage):
        self.assertFalse(storage.tracks.empty)
        self.assertTrue(storage.file_name.exists())

    def _get_value(self, frame: pd.DataFrame, hashed_value: int) -> int:
        line = _get_hash(frame, hashed_value)
        self.assertFalse(line.empty)
        return list(line['value'])[0]


def _hash(value: int) -> str:
    return sha3_224(bytearray([value])).hexdigest()[:32]


def _create_csv():
    raw_data = _create_dataframe((0, 1))
    csv_file = NamedTemporaryFile(mode='w+t', suffix='.csv')
    raw_data.to_csv(csv_file)
    csv_file.flush()
    return csv_file


def _create_dataframe(values: Tuple[int, ...]) -> pd.DataFrame:
    raw_data = {
        'hash': (_hash(i) for i in values),
        'value': values
    }
    return pd.DataFrame.from_dict(raw_data)


def _get_hash(frame: pd.DataFrame, hashed_value: int) -> pd.DataFrame:
    return frame.loc[frame['hash'] == _hash(hashed_value)]


def _empty_storage(**kwargs) -> TrackDataStorage:
    with NamedTemporaryFile('r', suffix='.csv') as temp_file:
        return TrackDataStorage(Path(temp_file.name), **kwargs)


def _mock_track(value: int) -> Mock:
    new_track = Mock()
    new_track.hash = _hash(value)
    new_track.data_frame = _create_dataframe((value,))
    return new_track
