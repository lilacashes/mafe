__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

import unittest
from pathlib import Path
from unittest.mock import MagicMock, Mock, patch
from tempfile import TemporaryDirectory

from mafe.scan import Scanner
from mafe.scan.scanner import cut, is_audio


class ScannerTestCase(unittest.TestCase):

    def test_scanner_with_empty_dir(self):
        with TemporaryDirectory() as temp_dir:
            scanner = Scanner(Mock(), [temp_dir], quiet=True)
        self.assertEqual([], scanner.files)
        self.assertEqual(0, scanner.done)
        self.assertEqual(0, scanner.failed)

    def test_progress_string_contains_done_and_fail(self):
        with TemporaryDirectory() as temp_dir:
            scanner = Scanner(Mock(), [temp_dir], quiet=True)
        self.assertIn('0 ok', scanner.progress())
        self.assertIn('0 fail', scanner.progress())
        self.assertIn('0 tot', scanner.progress())

    def test_print_info(self):
        with TemporaryDirectory() as temp_dir:
            scanner = Scanner(Mock(), [temp_dir], quiet=False)
        with patch('builtins.print') as mock_print:
            scanner.print_info(MagicMock())
        self.assertTrue(mock_print.mock_calls)
        self.assertIn('0 ok', str(mock_print.mock_calls[0]))
        self.assertIn('0 fail', str(mock_print.mock_calls[0]))
        self.assertIn('0 tot', str(mock_print.mock_calls[0]))

    def test_print_info_doesnt_print_if_quiet(self):
        with TemporaryDirectory() as temp_dir:
            scanner = Scanner(Mock(), [temp_dir], quiet=True)
        with patch('builtins.print') as mock_print:
            scanner.print_info(MagicMock())
        self.assertFalse(mock_print.mock_calls)

    def test_log_problem(self):
        with TemporaryDirectory() as temp_dir:
            scanner = Scanner(Mock(), [temp_dir], quiet=False)
        with patch('builtins.print') as mock_print:
            scanner.log_problem(MagicMock(), ValueError())
        self.assertEqual(1, scanner.failed)
        self.assertTrue(mock_print.mock_calls)
        self.assertIn('0 ok', str(mock_print.mock_calls[0]))
        self.assertIn('1 fail', str(mock_print.mock_calls[0]))
        self.assertIn('0 tot', str(mock_print.mock_calls[0]))

    def test_log_problem_doesnt_print_if_quiet(self):
        with TemporaryDirectory() as temp_dir:
            scanner = Scanner(Mock(), [temp_dir], quiet=True)
        with patch('builtins.print') as mock_print:
            scanner.log_problem(MagicMock(), ValueError())
        self.assertFalse(mock_print.mock_calls)

    def test_cut(self):
        self.assertEqual('*' * 10, cut('*' * 10, 10))
        self.assertEqual('*' * 9 + ' ', cut('*' * 9, 10))
        self.assertEqual('*' * 7 + '...', cut('*' * 11, 10))
        self.assertEqual('*' * 7 + '...', cut('*' * 11, 9, min_width=10))

    def test_is_audio_with_extensions(self):
        self.assertTrue(is_audio(_path_with_extension('mp3')))
        self.assertTrue(is_audio(_path_with_extension('MP3')))
        self.assertTrue(is_audio(_path_with_extension('ogg')))
        self.assertTrue(is_audio(_path_with_extension('m4a')))
        self.assertTrue(is_audio(_path_with_extension('wav')))
        self.assertTrue(is_audio(_path_with_extension('aif')))
        self.assertTrue(is_audio(_path_with_extension('aiff')))

    @patch('magic.from_file', return_value='audio/xxx')
    def test_is_audio_from_magic_audio(self, _):
        self.assertTrue(is_audio(_path_with_extension('xxx')))

    @patch('magic.from_file', return_value='video/xxx')
    def test_is_audio_from_magic_not_audio(self, _):
        self.assertFalse(is_audio(_path_with_extension('xxx')))


def _path_with_extension(ext: str) -> Path:
    return Path('audio.' + ext)
