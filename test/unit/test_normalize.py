__author__ = 'Lene Preuss <lene.preuss@gmail.com>'

from copy import deepcopy
from typing import Dict

import pandas as pd

from mafe.process.normalize import normalize

DEFAULT_NUMBER_VALUES = dict(
    A=[10, 5, 7], B=[0.5, 0.35, 0.25]
)
NEGATIVE_NUMBER_VALUES = dict(
    A=[10, 5, -10], B=[0.5, 0.35, -0.25]
)
DEFAULT_TEXT_VALUES = dict(text=['foo', 'bar', 'baz'])
EXPECTED_ZERO_MAX_VALUES = dict(
    A=[1, 0.5, 0.7], B=[1, 0.7, 0.5]
)
EXPECTED_MIN_MAX_VALUES = dict(
    A=[1, 0, 0.4], B=[1, 0.4, 0]
)
EXPECTED_MEAN_VALUES = dict(
    A=[1.059626, -0.927173, -0.132453], B=[1.059626, -0.132453, -0.927173]
)
EXPECTED_NEGATIVE_VALUES = dict(
    A=[1, 0.75, 0], B=[1, 0.8, 0]
)


def test_norm_zero_max_without_text():
    frame = pd.DataFrame.from_dict(DEFAULT_NUMBER_VALUES)
    normed_data = pd.DataFrame.from_dict(EXPECTED_ZERO_MAX_VALUES)
    check_normalization(frame, normed_data, 'zero_max', tuple())


def test_norm_zero_max_with_text():
    frame = with_text_columns(DEFAULT_NUMBER_VALUES)
    normed_data = with_text_columns(EXPECTED_ZERO_MAX_VALUES)
    check_normalization(frame, normed_data, 'zero_max', ('text',))


def test_norm_min_max_without_text():
    frame = pd.DataFrame.from_dict(DEFAULT_NUMBER_VALUES)
    normed_data = pd.DataFrame.from_dict(EXPECTED_MIN_MAX_VALUES)
    check_normalization(frame, normed_data, 'min_max', tuple())


def test_norm_min_max_with_text():
    frame = with_text_columns(DEFAULT_NUMBER_VALUES)
    normed_data = with_text_columns(EXPECTED_MIN_MAX_VALUES)
    check_normalization(frame, normed_data, 'min_max', ('text',))


def test_norm_mean_without_text():
    frame = pd.DataFrame.from_dict(DEFAULT_NUMBER_VALUES)
    normed_data = pd.DataFrame.from_dict(EXPECTED_MEAN_VALUES)
    check_normalization(frame, normed_data, 'mean', tuple())


def test_norm_mean_with_text():
    frame = with_text_columns(DEFAULT_NUMBER_VALUES)
    normed_data = with_text_columns(EXPECTED_MEAN_VALUES)
    check_normalization(frame, normed_data, 'mean', ('text',))


def test_norm_min_max_negative():
    frame = pd.DataFrame.from_dict(NEGATIVE_NUMBER_VALUES)
    normed_data = pd.DataFrame.from_dict(EXPECTED_NEGATIVE_VALUES)
    check_normalization(frame, normed_data, 'min_max', tuple())


def test_norm_zero_max_negative():
    frame = pd.DataFrame.from_dict(NEGATIVE_NUMBER_VALUES)
    normed_data = pd.DataFrame.from_dict(EXPECTED_NEGATIVE_VALUES)
    check_normalization(frame, normed_data, 'zero_max', tuple())


def with_text_columns(cols: Dict) -> pd.DataFrame:
    data = deepcopy(cols)
    data.update(DEFAULT_TEXT_VALUES)
    return pd.DataFrame.from_dict(data)


def check_normalization(frame, expected, method, text_cols):
    normalized = normalize(frame, method=method, text_cols=text_cols)
    pd.testing.assert_frame_equal(expected, normalized)
