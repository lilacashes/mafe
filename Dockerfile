FROM python:3.8

RUN apt-get -y -qq update
RUN apt-get -y -q install libsndfile1 ffmpeg > /dev/null

# Create virtual environment.
RUN mkdir -p /mafe/venv
RUN python -m venv /mafe/venv

# Activate the venv.
ENV VIRTUAL_ENV /mafe/venv
ENV PATH "$VIRTUAL_ENV/bin:${PATH}"

# Install poetry into the venv.
RUN pip install -q poetry

# Install dependencies into the venv.
COPY poetry.lock pyproject.toml /mafe/
WORKDIR /mafe
RUN poetry install

CMD bash