stages:
  - build-image
  - test
  - pipeline:prepare
  - pipeline:scan
  - pipeline:normalize
  - pipeline:pca
  - pipeline:evaluate
  - publish

include:
  - template: Code-Quality.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml

.tests:
  stage: test
  image: registry.gitlab.com/lilacashes/mafe/build_env:latest
  except:
    changes:
      - Dockerfile
      - poetry.lock
  needs: []
  variables:
    VIRTUAL_ENV: /mafe/venv
  before_script:
    - poetry config virtualenvs.create false

CreateDockerImage:
  stage: build-image
  image: docker:19.03.13
  only:
    changes:
      - Dockerfile
      - poetry.lock
  services:
    - docker:19.03.13-dind
  script:
    - docker build -t registry.gitlab.com/lilacashes/mafe/build_env .
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} registry.gitlab.com
    - docker push registry.gitlab.com/lilacashes/mafe/build_env

nosetests:
  extends: .tests
  script:
    - poetry run nosetests --with-xunit --with-coverage --cover-package=mafe.scan,mafe.process --cover-min-percentage=90 --cover-xml test | tee .nosetest
  coverage: '/^TOTAL\s+\d+\s+\d+\s+(\d+\%)$/'
  artifacts:
    paths:
      - coverage.xml
    reports:
      junit: nosetests.xml

mypy:
  extends: .tests
  script:
  - poetry run mypy .

flake8:
  extends: .tests
  variables:
    REPORT_FILE: .code-quality.json
  script:
    - poetry run flake8 . --output-file=.code-quality.json
  artifacts:
    reports:
      codequality: .code-quality.json

pylint:
  extends: .tests
  script:
    - poetry run pylint --output-format=text --reports y test mafe | tee .pylint-scan_library.txt
  artifacts:
    reports:
      codequality: .pylint-scan_library.txt

GenerateSimulationData:
  extends: .tests
  stage: pipeline:prepare
  except:
    refs:
      - tags
  needs: []
  script:
    - cp -a test/integration/data/sound/ogg test_data
    - for dir in test_data/cough test_data/default; do
    -   pushd $dir
    -   for file in *.ogg; do
    -     ffmpeg -i $file \
            $(basename $file).mp3 $(basename $file).flac $(basename $file).m4a \
            $(basename $file).aif $(basename $file).wav
    -   done
    -   popd
    - done
  artifacts:
    paths:
      - test_data

SimulateScan:
  extends: .tests
  stage: pipeline:scan
  except:
    refs:
      - tags
  needs:
    - job: GenerateSimulationData
      artifacts: true
  script:
    - time poetry run mafe -t test_scan.csv.bz2 scan -f test_data
  artifacts:
    paths:
      - test_scan.csv.bz2

SimulateNormalization:
  extends: .tests
  stage: pipeline:normalize
  except:
    refs:
      - tags
  needs:
    - job: SimulateScan
      artifacts: true
  script:
    - time poetry run mafe -t test_scan.csv.bz2 -o test_normalized.csv.bz2 normalize
  artifacts:
    paths:
      - test_normalized.csv.bz2

SimulatePCA:
  extends: .tests
  stage: pipeline:pca
  except:
    refs:
      - tags
  needs:
    - job: SimulateNormalization
      artifacts: true
  script:
    - time poetry run mafe -t test_normalized.csv.bz2 -o test_reduced.csv.bz2 pca
  artifacts:
    paths:
      - test_reduced.csv.bz2

SimulateDistance:
  extends: .tests
  stage: pipeline:evaluate
  except:
    refs:
      - tags
  needs:
    - job: SimulateNormalization
      artifacts: true
  script:
    - time poetry run mafe -t test_normalized.csv.bz2 -o test_distances.csv.bz2 distance
    - bzcat test_distances.csv.bz2 | tail -n 5
  artifacts:
    paths:
      - test_distances.csv.bz2

SimulateCluster:
  extends: .tests
  stage: pipeline:evaluate
  except:
    refs:
      - tags
  needs:
    - job: SimulateNormalization
      artifacts: true
  script:
    - time poetry run mafe -t test_normalized.csv.bz2 -o test_cluster.csv.bz2 cluster -n 4
    - bzcat test_cluster.csv.bz2 | tail -n 5
  artifacts:
    paths:
      - test_cluster.csv.bz2

SimulateClusterReduced:
  extends: .tests
  stage: pipeline:evaluate
  except:
    refs:
      - tags
  needs:
    - job: SimulatePCA
      artifacts: true
  script:
    - time poetry run mafe -t test_reduced.csv.bz2 -o test_cluster_reduced.csv.bz2 cluster -n 4 -V -I cluster.png
    - bzcat test_cluster_reduced.csv.bz2 | tail -n 5
    - test -f cluster.png
    - ls -l cluster.png
  artifacts:
    paths:
      - test_cluster_reduced.csv.bz2

SimulatePipelineWhole:
  extends: .tests
  stage: pipeline:evaluate
  except:
    refs:
      - tags
  needs:
    - job: GenerateSimulationData
      artifacts: true
  script:
    - time poetry run mafe -t test_scan.csv.bz2 scan -f test_data
    - time poetry run mafe -t test_scan.csv.bz2 -o test_normalized.csv.bz2 normalize
    - time poetry run mafe -t test_normalized.csv.bz2 -o test_distances_all.csv.bz2 distance
    - bzcat test_distances_all.csv.bz2 | tail -n 5
    - time poetry run mafe -t test_normalized.csv.bz2 -o test_cluster_all.csv.bz2 cluster -n 4
    - bzcat test_cluster_all.csv.bz2 | tail -n 5
    - time poetry run mafe -t test_normalized.csv.bz2 -o test_reduced_all.csv.bz2 pca
    - bzcat test_reduced_all.csv.bz2 | tail -n 5
    - time poetry run mafe -t test_reduced_all.csv.bz2 -o test_cluster_reduced.csv.bz2 cluster -n 4 -V -I cluster.png
    - bzcat test_cluster_reduced.csv.bz2 | tail -n 5
    - test -f cluster.png
    - ls -l cluster.png
  artifacts:
    paths:
      - test_distances_all.csv.bz2
      - test_cluster_all.csv.bz2
      - test_reduced_all.csv.bz2
      - test_cluster_reduced.csv.bz2

CreateTag:
  stage: publish
  image: python:3.8
  needs:
    - job: pylint
      artifacts: false
    - job: flake8
      artifacts: false
    - job: mypy
      artifacts: false
    - job: nosetests
      artifacts: false
    - job: SimulatePipelineWhole
      artifacts: false
    - job: SimulateClusterReduced
      artifacts: false
    - job: SimulateCluster
      artifacts: false
    - job: SimulateDistance
      artifacts: false
    - job: SimulatePCA
      artifacts: false
    - job: SimulateNormalization
      artifacts: false
    - job: SimulateScan
      artifacts: false
  only:
    refs:
      - master
    variables:
      - $GITLAB_ACCESS_TOKEN
  except:
    changes:
      - Dockerfile
      - poetry.lock
  allow_failure: true
  script:
    - VERSION=$(egrep 'version = ".*"' pyproject.toml | cut -d \" -f 2)
    - echo "**** Tagging release as version $VERSION"
    - git config user.email "${GITLAB_USER_EMAIL}"
    - git config user.name "${GITLAB_USER_NAME}"
    - git remote add tag-origin https://oauth2:${GITLAB_ACCESS_TOKEN}@gitlab.com/${CI_PROJECT_PATH}
    - git tag -a "${VERSION}" -m "Released $(date +%Y-%m-%d)"
    - git push tag-origin "${VERSION}"

PublishToPyPI:
  stage: publish
  image: python:3.8
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD
  script:
    - VERSION=$(egrep 'version = ".*"' pyproject.toml | cut -d \" -f 2)
    - test "${CI_COMMIT_TAG}" == "${VERSION}" || exit 1
    - echo "**** Upgrading to ${VERSION}"
    - pip install -q poetry
    - poetry build
    - poetry config repositories.testpypi https://test.pypi.org/legacy/
    - poetry publish --username ${PYPI_USERNAME} --password ${PYPI_PASSWORD} --repository testpypi
    - echo "**** Attempting pip install from test PyPI server"
    - apt-get -y -qq update
    - apt-get -y -q install libsndfile1 ffmpeg > /dev/null
    - pip install -q --index-url https://test.pypi.org/simple --extra-index-url https://pypi.org/simple mafe
    - pip freeze | grep mafe
    - echo "**** Publishing on live PyPI server"
    - poetry publish --username ${PYPI_USERNAME} --password ${PYPI_PASSWORD}
